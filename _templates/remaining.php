<div class="header-content clearfix">
<div class="header-image" style="size: 100px;">
<a href="https://sibidharan.me/">
<!-- <img src="//sibidharan.me/wp-content/uploads/2020/06/Logo-round.png" alt="image"> -->
</a>
</div>

<!-- Text Logo -->
<div class="text-logo">
<a href="https://sibidharan.me/" >
<div class="logo-text">Sibidharan <span>Nandhakumar</span></div>
</a>
</div>
<!-- /Text Logo -->
               
<!-- Navigation -->
<div class="site-nav mobile-menu-hide">
<ul id="menu-classic-menu" class="leven-classic-menu site-main-menu"><li id="menu-item-409" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-8 current_page_item menu-item-409"><a href="https://sibidharan.me/" aria-current="page" data-hover="1">Home</a></li>
<li id="menu-item-174" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-174"><a href="https://sibidharan.me/about-me/" data-hover="1">About Me</a></li>
<li id="menu-item-1662" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1662"><a href="https://sibidharan.me/verify-cert/" data-hover="1">Certificate Verification</a></li>
<li id="menu-item-191" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-191"><a href="https://sibidharan.me/contact/" data-hover="1">Contact</a></li>
</ul>               
 </div>

<a class="menu-toggle mobile-visible">
<i class="fa fa-bars"></i>
</a>
</div>