<?php
include 'libs/load.php';

load_template('_header');?>
<html>
    

<link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/pricing/">

<!-- Bootstrap core CSS -->
<link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="pricing.css" rel="stylesheet">


<h1 style="position: absolute;left: 46%;top: 15%;">About Me</h1>

<body style="background: linear-gradient(45deg, #3a4a66, #021016);">

<div id="content" class="page-content site-content single-post" role="main">
                
<article id="post-171" class="post-171 page type-page status-publish hentry">
	<div class="entry-content">
		<div class="fw-page-builder-content"><section class="fw-main-row  ">
	<div class="fw-container">
		<div class="row">
	

<div class=" col-xs-12 col-sm-12 ">
    <div id="col_inner_id-6615111482796" class="fw-col-inner" data-paddings="0px 0px 0px 0px">
		
<div class="block-title">
    <h2>What I Do</h2>
</div>
	</div>
</div>
</div>

<div class="row">
	

<div class=" col-xs-12 col-sm-6 ">
    <div id="col_inner_id-661511148280f" class="fw-col-inner" data-paddings="0px 0px 0px 0px">
		
<div id="info-list-6615111482848" class="info-list-w-icon">
	
			<div class="info-block-w-icon">
		    <div class="ci-icon">
		        		        	                        <i class="fa fa-desktop"></i>
		                		    </div>
		    <div class="ci-text">
		        <h4>Design and Develop</h4>
		        		        <p>With 13 years of Industrial Experience, I can design any complicated software and apps for your needs that can get your business or idea up and running.</p>
		        		    </div>
		</div>
			<div class="info-block-w-icon">
		    <div class="ci-icon">
		        		        	                        <i class="fa fa-server"></i>
		                		    </div>
		    <div class="ci-text">
		        <h4>Architect</h4>
		        		        <p>I can architect any complicated servers and networked infrastructures online or on-premises for your application or software or hardware or IoT needs.</p>
		        		    </div>
		</div>
	</div>
	</div>
</div>


<div class=" col-xs-12 col-sm-6 ">
    <div id="col_inner_id-6615111482879" class="fw-col-inner" data-paddings="0px 0px 0px 0px">
		
<div id="info-list-6615111482893" class="info-list-w-icon">
	
			<div class="info-block-w-icon">
		    <div class="ci-icon">
		        		        	                        <i class="fa fa-mortar-board"></i>
		                		    </div>
		    <div class="ci-text">
		        <h4>Teach and Train</h4>
		        		        <p>I have reached an impasse, and I cannot go up further without taking my community up to my level. So I am teaching all that I know from time to time.</p>
		        		    </div>
		</div>
			<div class="info-block-w-icon">
		    <div class="ci-icon">
		        		        	                        <i class="fa fa-puzzle-piece"></i>
		                		    </div>
		    <div class="ci-text">
		        <h4>Hustle</h4>
		        		        <p>There is no one thing. I hustle a lot to explore opportunities and learn various domains of science, technology and engineering to apply them in my work.</p>
		        		    </div>
		</div>
	</div>
	</div>
</div>
</div>



	</div>
</section>
</div>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
            </div>

</body>

</html>
